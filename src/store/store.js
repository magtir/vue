import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        products: []
    },
    getters: {
        getProducts(state) {
            return state.products;
        }
    },
    actions: {
        loadProducts(ctx, products) {
            ctx.commit('loadProducts', products)
        }
    },
    mutations: {
        loadProducts(state, payload) {
            state.products = payload;
        }
    }
});