import ProductsList from './ProductsList'
import ProductView from './ProductView';
import ProductAdd from './ProductAdd'

export const routes = [
    {
        path: '/',
        name: 'index',
        component: ProductsList,
    },
    {
        path: '/add',
        name: 'add',
        component: ProductAdd,
    },
    {
        path: '/view/:id',
        name: 'view',
        component: ProductView,
        props: {staticText: 'some text'}
    },
    {
        path: '*',
        component: ProductsList
    }
];