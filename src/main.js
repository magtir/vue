import Vue from 'vue'
import App from './App.vue'
import {router} from './route'
import store from './store/store'

Vue.filter('reverse', function(val, symbol = '') {
    return val.split('').reverse().join(symbol);
});

new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App)
});
